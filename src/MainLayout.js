import React from 'react';
import './App.css';
import store from "./Redux/Store";
import { connect } from 'react-redux'
import { switchName } from './Redux/Actions'



function MainLayout(props) {

    const handleChange = (e) =>{
        const { dispatch } = props
        console.log(props)
        dispatch(switchName(e.target.value))
    }

    return (

            <div className="main__page">
                <div>
                    {console.log(store.getState())}
                    <input onChange={(e)  => handleChange(e) }/>
                </div>
                <div>
                    {props.name}
                </div>
            </div>
    );

}
function mapStateToProps(state) {
    const { name } = state.switchHandle
    return {
        name,
    }
}

export default connect(mapStateToProps)(MainLayout)
