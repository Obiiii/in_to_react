export const SWITCH_NAME = 'SWITCH_NAME'

export const switchName = name =>{
    return {
        type:"SWITCH_NAME",
        name: name
    }
}
