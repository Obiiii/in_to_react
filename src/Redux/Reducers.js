import {switchName} from "./Actions";
import {combineReducers} from "redux";
import {SWITCH_NAME} from "./Actions";

const switchHandle = (state={},action) => {

    switch (action.type) {
        case SWITCH_NAME:

            return {
                ...state,
                name: action.name
            }
        default:
            return state;
    }

}

const rootreducers = combineReducers({switchHandle})

export default rootreducers