import React from 'react';
import './App.css';
import store from "./Redux/Store";
import { Provider } from 'react-redux'
import { connect } from 'react-redux'
import { switchName } from './Redux/Actions'
import MainLayout from "./MainLayout";



function App(props) {

  const handleChange = (e) =>{
    store.dispatch(switchName(e.target.value))
  }

  return (
      <Provider store={store}>
          <MainLayout />
      </Provider>
  );

}


export default App
