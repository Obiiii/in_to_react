import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const rend = () => ReactDOM.render(<App />, document.getElementById('root'));

rend();

serviceWorker.unregister();
